package main

import "fmt"

type node struct {
	num int
	mx  int
}

type Stack []node
var Mx int = -1
func (s *Stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *Stack) Push(num int) {
	if s.IsEmpty() {
		n,m:= num,num
		Mx = m
		Node := node{n,m}
		*s = append(*s, Node)
	} else {
		if num > Mx {
			Mx = num
		}
		n:=num
		Node := node{n,Mx}
		*s = append(*s, Node)
	}
}

func (s *Stack) Pop() (node, bool) {
	if s.IsEmpty() {
		return node{-1,-1}, false
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element, true
	}
}
func (s *Stack) GetMax() (int) {
	if s.IsEmpty() {
		return -1
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element.mx
	}
}

func main() {
	var stack Stack

	stack.Push(1)
	stack.Push(2)
	stack.Push(3)
	stack.Push(2)

	for len(stack) > 0 {
		x, y := stack.Pop()
		if y == true {
			fmt.Println(x)
		}
	}
}